package com.example.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author qucl
 * @date 2019/3/13 11:09
 */
@RestController
@RequestMapping("hello")
public class HelloController {
    @GetMapping
    public String sayHello() {
        return "hello0504";
    }
}
