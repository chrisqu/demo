package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoApplication {

    public static void main(String[] args) {
        ApplicationUtil.setApplicationContext(SpringApplication.run(DemoApplication.class, args));
    }

}

