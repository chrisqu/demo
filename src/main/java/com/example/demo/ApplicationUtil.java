package com.example.demo;

import org.springframework.context.ApplicationContext;

/**
 * Created by liuzx on 2018/3/27.
 */
public class ApplicationUtil {

    private static ApplicationContext applicationContext;

    public static void setApplicationContext(ApplicationContext context) {
        applicationContext = context;
    }

    public static <T> T getBean(Class<T> clazz) {
        return applicationContext.getBean(clazz);
    }
}
